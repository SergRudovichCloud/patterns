import { showModal } from "./modal.mjs"
import { createElement } from './dom-helpers.mjs';

function showResults(gameResults, closeModal) {
    const results = createElement({ tagName: 'div', className: 'results' });
    for (let i = 1; i <= gameResults.length; i++) {
       const userResult = createElement({
            tagName: 'div',
            className: "user-result",
            attributes: {
                id: `place-${i}`
            }
        });
        userResult.innerHTML = `${i} - ${gameResults[i-1]}`
        results.appendChild(userResult);
    }
    showModal({
        title: "Результаты клавогонки",
        bodyElement: results,
        onClose: closeModal
    });
}

export { showResults }