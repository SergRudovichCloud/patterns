import { createElement } from "./dom-helpers.mjs";
import { username, socket } from "../const.mjs"

const roomElement = (usersCount, roomId) => {
  const roomDiv = createElement({
    tagName: "div",
    className: "room",
    attributes: {}
  });

  roomDiv.innerHTML = `
  <span>${usersCount} users connected</span>
  <h4>${roomId}</h4>
  `;

  const roomJoinButton = createElement({
    tagName: "button",
    className: "join-btnt",
    attributes: { id: roomId }
  });

  roomJoinButton.innerText = "Join";

  const onJoinRoom = () => {
    socket.emit("JOIN_ROOM", { roomName: roomId, username });
  };

  roomJoinButton.addEventListener("click", onJoinRoom);

  roomDiv.appendChild(roomJoinButton);

  return roomDiv;
}

export { roomElement }