import { createElement } from "./dom-helpers.mjs";
import { bot } from "../const.mjs";

const botSay = (comment) => {
    if (comment !== null) {
        const say = createElement({
            tagName: "span",
            className: "comment",
            attributes: {}
        });
        say.innerHTML = comment;
        bot.appendChild(say);
    } else {
        bot.innerHTML = "";
    }
}

export { botSay }