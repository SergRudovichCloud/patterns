import { userElement } from "./user-element.mjs"
import {
  usersContainer
} from "../const.mjs";

function viewRoom(users, you) {
  let mi = '';
  usersContainer.innerHTML = '';
  users.forEach(user => {
    if (user == you) {
      mi = ' (you) '
    } else {
      mi = ''
    };
    const userHTML = userElement(user, mi);
    usersContainer.appendChild(userHTML);
  });

}

export { viewRoom }