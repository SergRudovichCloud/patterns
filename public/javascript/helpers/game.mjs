import { socket, username, textDone, textUndone, textNext } from "../const.mjs"

function startGame(gameText, roomName) {
    document.addEventListener('keyup', keyboardEventsHandle);
  
    textUndone.innerHTML = gameText;
  
    gameText += " ";
  
    let GameCount = 0;
  
    function keyboardEventsHandle(e) {
      if (e.key === gameText[GameCount]) {
        GameCount++;
        socket.emit("USER_TYPE", { progress: GameCount, roomId: roomName, user: username });
        if (GameCount === gameText.length) {
          document.removeEventListener('keyup', keyboardEventsHandle);
        }
        textDone.innerHTML = gameText.slice(0, GameCount);
        textUndone.innerHTML = gameText.slice(GameCount + 1);
        if (GameCount < gameText.length)
          textNext.innerHTML = gameText[GameCount];
      }
    }
  }

  export { startGame }