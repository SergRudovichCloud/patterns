import { addClass, removeClass } from "./dom-helpers.mjs";

function setUserStatus(status, user) {
    const userStatus = document.getElementsByClassName(`user-status-${user}`)[0];
    if (status) {
        addClass(userStatus, "ready-status-green");
        removeClass(userStatus, "ready-status-red");
    } else {
        addClass(userStatus, "ready-status-red");
        removeClass(userStatus, "ready-status-green");
    }
}

export { setUserStatus }