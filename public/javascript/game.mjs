import { addClass, removeClass } from "../javascript/helpers/dom-helpers.mjs";
import { viewRoom } from "./helpers/view-room.mjs";
import { setUserStatus } from "./helpers/set-user-status.mjs";
import { roomElement } from "./helpers/room-element.mjs";
import { botSay } from "./helpers/actions.mjs";
import { startGame } from "./helpers/game.mjs";
import { showResults } from "./helpers/show-results.mjs";

import {
  createRoomBtn,
  roomsPage,
  gamePage,
  roomsGrid,
  roomNameLabel,
  quitRoomBtn,
  readyBtn,
  timer,
  gameTimer,
  textDone,
  textNext,
  textUndone,
  username,
  socket
} from "./const.mjs";

const gameState = {
  roomName: "",
  text: "the good",
  isGame: false
}

if (!username) {
  window.location.replace("/login");
}

createRoomBtn.addEventListener('click', () => {
  gameState.roomName = prompt("Input Room Name:");
  socket.emit("CREATE_ROOM", { roomName: gameState.roomName, username });
  removeClass(gamePage, "display-none");
  addClass(roomsPage, "display-none");
})

quitRoomBtn.addEventListener('click', () => {
  socket.emit("LEAVE_ROOM", ({ roomName: gameState.roomName, username }));
  addClass(gamePage, "display-none");
  removeClass(roomsPage, "display-none");
  gameState.roomName = "";
})

readyBtn.addEventListener('click', () => {
  let status = false;
  if (readyBtn.innerText === "READY") {
    status = true;
    readyBtn.innerText = 'NOT READY'
  } else {
    status = false;
    readyBtn.innerText = 'READY'
  }
  setUserStatus(status, username);
  socket.emit("USER_IS_READY", { status, roomId: gameState.roomName, user: username });
})

socket.on("DUBLICATE_USER", username => {
  sessionStorage.removeItem("username");
  window.location.replace("/login");
  alert(`Usrename ${username} exist`);
});

socket.on("ROOMS_UPDATE", rooms => {
  roomsGrid.innerHTML = '';
  rooms.forEach(room => {
    const roomCard = roomElement(room[1], room[0]);
    roomsGrid.appendChild(roomCard);
  });
});

socket.on("DUBLICATE_ROOM", roomId => {
  alert(`Room ${roomId} exist`);
});

socket.on("JOIN_ROOM_DONE", ({ roomId, users }) => {
  removeClass(gamePage, "display-none");
  addClass(roomsPage, "display-none");
  roomNameLabel.innerHTML = roomId;
  gameState.roomName = roomId;
  viewRoom(users, username);
});

socket.on("USER_IS_READY", ({ status, user }) => setUserStatus(status, user));

socket.on("TIMER_START", () => {
  botSay(null);
  addClass(quitRoomBtn, "display-none");
  addClass(readyBtn, "display-none");
});
socket.on("TIMER_TIK", tik => timer.innerHTML = tik);
socket.on("GAME_START", (gameTxt) => {
  gameState.isGame = true;
  addClass(timer, "display-none");
  gameState.text = gameTxt;
  startGame(gameState.text, gameState.roomName);
});
socket.on("GAME_TIMER_TIK", tik => {
  gameTimer.innerHTML = `Game timer: ${tik} `;
});

socket.on("USER_PROGRESS", ({ progress, user, roomId }) => {
  const userProgress = document.getElementsByClassName(`user-progress-${user}`)[0];
  userProgress.style.width = `${progress * (100 / gameState.text.length)}%`;
  if (progress * (100 / gameState.text.length) >= 100) {
    if (user == username) socket.emit("USER_TEXT_DONE", ({ user, roomId }));
    userProgress.style.backgroundColor = "blue";
  }
});

const closeModal = () => {
  removeClass(quitRoomBtn, "display-none");
  removeClass(readyBtn, "display-none");
}

socket.on("GAME_END", gameResult => {
  gameState.isGame = false;
  gameTimer.innerHTML = "";
  showResults(gameResult, closeModal);

  [...document.getElementsByClassName("status")]
    .forEach(element => {
      addClass(element, "ready-status-red");
      removeClass(element, "ready-status-green");
    });
  readyBtn.innerText = "READY";
  [...document.getElementsByClassName("user-progress")]
    .forEach(element => element.style.width = "0px");
  textDone.innerHTML = "";
  textUndone.innerHTML = "";
  textNext.innerHTML = "";
});



socket.on("BOT", botSay);