import * as config from "./config";
import * as L from "./lib";
import { db } from "./servises/db.mjs";
import Commentator from "./servises/commentator.mjs";
import { startTimer, stopGameTimer } from "./servises/timers.mjs";
import { finishGame } from "./servises/emits.mjs";

let botTxt = require('./comments.json');

const bot = new Commentator(db, "Комментатор", botTxt);
const activUsers = new Map();

export default io => {
  io.on("connection", socket => {

    const leaveRoom = ({ roomName, username }) => {
      socket.leave(roomName);
      db.delUserFromRoom(roomName, username);
      let users = db.getRoomUsers(roomName);
      if (users.length === 0) {
        db.delRoom(roomName);
        socket.broadcast.emit("ROOMS_UPDATE", db.getRooms());
        socket.emit("ROOMS_UPDATE", db.getRooms());
      } else {
        if (db.isAllReadyInRoom(roomName)) {
          socket.to(roomName).emit("JOIN_ROOM_DONE", { roomId: roomName, users });
          io.in(roomName).emit("TIMER_START");
          io.in(roomName).emit("BOT", bot.say("TIMER_START", roomName));
          startTimer(config.SECONDS_TIMER_BEFORE_START_GAME, roomName, io, username, bot, socket );
        }
        socket.broadcast.emit("ROOMS_UPDATE", db.getRooms());
        socket.emit("ROOMS_UPDATE", db.getRooms());
        socket.to(roomName).emit("JOIN_ROOM_DONE", { roomId: roomName, users });
      }
    };

    const username = socket.handshake.query.username;

    if (L.hasItem(username, activUsers)) {
      socket.emit("DUBLICATE_USER", username);
    } else {
      activUsers.set(socket.id, username);
      socket.emit("ROOMS_UPDATE", db.getRooms());
    }

    socket.on('disconnect', () => {
      const user = activUsers.get(socket.id);
      activUsers.delete(socket.id);
      const roomId = db.roomWhereUserIs(user);
      if (roomId) leaveRoom({ roomName: roomId, username: user });
    });
    socket.on("CREATE_ROOM", ({ roomName, username }) => {
      if (db.hasRoom(roomName)) {
        socket.emit("DUBLICATE_ROOM", roomName);
      } else {
        db.setRoom(roomName, username);
        socket.join(roomName);
        const users = [];
        users.push(username);
        socket.emit("JOIN_ROOM_DONE", { roomId: roomName, users });
        socket.broadcast.emit("ROOMS_UPDATE", db.getRooms());
      }
    });
    socket.on("JOIN_ROOM", ({ roomName, username }) => {
        socket.join(roomName);
        db.addUserToRoom(roomName, username);
        const users = db.getRoomUsers(roomName);
        socket.broadcast.emit("ROOMS_UPDATE", db.getRooms());
        io.in(roomName).emit("JOIN_ROOM_DONE", { roomId: roomName, users });
    });
    socket.on("LEAVE_ROOM", leaveRoom);
    socket.on("USER_IS_READY", ({ status, roomId, user }) => {
      db.setUserStatus(status, roomId, user);
      socket.to(roomId).emit("USER_IS_READY", { status, user });
      if (db.isAllReadyInRoom(roomId)) {
        io.in(roomId).emit("TIMER_START");
        io.in(roomId).emit("BOT", bot.say("TIMER_START", roomId));
        startTimer(config.SECONDS_TIMER_BEFORE_START_GAME, roomId, io, user, bot, socket );
      }
    });
    socket.on("USER_TYPE", ({ progress, roomId, user }) => {
      db.setProgress(progress, roomId, user);
      io.in(roomId).emit("USER_PROGRESS", { progress, user, roomId });
    });
    socket.on("USER_TEXT_DONE", ({ user, roomId }) => {
      db.setTextDone(roomId, user);
      db.addFinishedUser(roomId, user);
      io.in(roomId).emit("BOT", bot.say("USER_TEXT_DONE", roomId, user));
      if (db.isAllTextDone(roomId)) {
        stopGameTimer();
        finishGame(io, roomId, bot);
      }
    });
  });
};
