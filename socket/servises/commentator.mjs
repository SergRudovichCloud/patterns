import * as L from "../lib";
export default class Commentator {
    constructor(db, name, botTxt) {
        this.db = db;
        this.name = name;
        this.botTxt = botTxt;
    }

    _timerStart(roomId) {
        let result = "";
        result =
            `${this.botTxt.timer_start_greetings}${this.name}!
             ${this.botTxt.timer_start_user}`
        this.db.getRoomUsers(roomId).forEach(user => {
            result += `${user}, `
        });
        result = result.slice(0, result.length - 2) + "!";
        return result;
    }

    _joke() {
        return (this.botTxt.joke[L.getRandom(0, this.botTxt.joke.length - 1)]);
    }

    _tik(roomId) {
        let progress = this.db.getUsersProgress(roomId);
        let result = "";
        result = `
        ${this.botTxt.user_progress1} 
        `;
        progress.forEach(user => {
            result += ` ${user[0]} ${this.botTxt.user_progress2} ${user[1]}`
        });
        return result;
    }

    _finish(roomId) {
        let result = "";
        let winners = this.db.getGameResult(roomId);
        result = `
        ${this.botTxt.finish1} ${winners[0]}
        `;
        return result;
    }

    say(action, roomId, user = null) {
        let result;
        switch (action) {
            case "TIMER_START":
                result = this._timerStart(roomId);
                break;
            case "GAME_START":
                result = `${this.botTxt.start1} ${user}. ${this.botTxt.start2}`;
                break;
            case "TIK":
                result = this._tik(roomId);
                break;
            case "JOKE":
                result = this._joke();
                break;
            case "USER_TEXT_DONE":
                result = `${this.botTxt.user_text_done1} ${user} ${this.botTxt.user_text_done2}`;
                break; SOON_FINISH
            case "SOON_FINISH":
                result = `${this.botTxt.soon_finish}`;
                break;
            case "GAME_END":
                result = this._finish(roomId);
                break;
        }
        return `${this.name}: ${result}`
    }
}
