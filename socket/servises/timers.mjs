import * as config from "../config";
import * as L from "../lib";
import { finishGame } from "./emits.mjs";
import { db } from "./db.mjs";

let gameTxt = require('../data.json');
let timerId, gameTimerId;

function startTimer(sec, roomId, io, user, bot, socket) {
  let tik = sec;
  let timerId = setInterval(() => {
    io.in(roomId).emit("TIMER_TIK", tik);
    tik--;
  }, 1000);
  setTimeout(() => {
    clearInterval(timerId);
    db.setRoomInGame(roomId);
    io.in(roomId).emit("GAME_START", gameTxt[L.getRandom(0, gameTxt.length - 1)]);
    socket.broadcast.emit("ROOMS_UPDATE", db.getRooms());
    io.in(roomId).emit("BOT", bot.say("GAME_START", roomId, user));
    startGame(roomId, io, bot, socket);
  }, (sec + 1) * 1000);
}

function startGame(roomId, io, bot, socket) {
  let tik = config.SECONDS_FOR_GAME;
  timerId = setInterval(() => {
    io.in(roomId).emit("GAME_TIMER_TIK", tik);
    if (tik % config.BOT_SAY_INTERVAL == 0)
      io.in(roomId).emit("BOT", bot.say("TIK", roomId));
    if (tik % config.BOT_SAY_JOKE_INTERVAL == 0)
      io.in(roomId).emit("BOT", bot.say("JOKE", roomId));
    if (tik == config.SECONDS_FOR_GAME / 10)
      io.in(roomId).emit("BOT", bot.say("SOON_FINISH", roomId));
    tik--;
  }, 1000);
  gameTimerId = setTimeout(() => {
    clearInterval(timerId);
    finishGame(io, roomId, bot, socket);
  }, (config.SECONDS_FOR_GAME + 1) * 1000);
}

function stopGameTimer() {
  clearInterval(timerId);
  clearTimeout(gameTimerId);
}

export { startTimer, stopGameTimer }