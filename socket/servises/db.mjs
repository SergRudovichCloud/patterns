import * as config from "../config";

class Db {
  constructor() {
    this.rooms = new Map();
    this.finishedGame = {};
  }

  _makeUser(isReady, progress, isTextDone) {
    return {
      isReady,
      progress,
      isTextDone
    }
  };

  setRoomInGame(roomId) {
    this.finishedGame[roomId] = [];

  }

  delRoomInGame(roomId) {
    delete this.finishedGame[roomId];
  }

  addFinishedUser(roomId, user) {
    this.finishedGame[roomId].push(user);
  }

  setRoom(roomId, user) {
    const users = new Map();
    this.rooms.set(roomId, users);
    this.addUserToRoom(roomId, user);
  }

  getRooms() {
    let result = [];
    for (let room of this.rooms.keys()) {
      if (this.getRoomUsers(room).length == config.MAXIMUM_USERS_FOR_ONE_ROOM) continue;
      if (this.finishedGame[room] !== undefined) continue;
      let item = [];
      item.push(room); item.push(this.getRoomUsers(room).length);
      result.push(item);
    }
    return result;
  }

  getRoomUsers(roomId) {
    const users = this.rooms.get(roomId);
    let result = [];
    for (let user of users.keys()) {
      result.push(user);
    }
    return result;
  }

  roomWhereUserIs(user) {
    let result = false;
    let rooms = this.getRooms();
    rooms.forEach(room => {
      if (this.getRoomUsers(room[0]).find(item => item === user)) result = room[0];
    });
    return result;
  }

  addUserToRoom(roomId, user) {
    const users = this.rooms.get(roomId);
    users.set(user, this._makeUser(false, 0, false));
    this.rooms.set(roomId, users);
  }

  delUserFromRoom(roomId, user) {
    const users = this.rooms.get(roomId);
    users.delete(user);
    this.rooms.set(roomId, users);
  }

  setUserStatus(status, roomId, user) {
    const users = this.rooms.get(roomId);
    const userToChange = users.get(user);
    userToChange.isReady = status;
    users.set(user, userToChange);
    this.rooms.set(roomId, users);
  }

  isAllReadyInRoom(roomId) {
    const users = this.rooms.get(roomId);
    let result = true;
    for (let user of users.values()) {
      if (user.isReady === false) result = false;
    }
    return result;
  }

  setAllNotReadyInRoom(roomId) {
    const users = this.rooms.get(roomId);
    for (let user of users.values()) {
      user.isReady = false;
    }
  }

  isAllTextDone(roomId) {
    const users = this.rooms.get(roomId);
    let result = true;
    for (let user of users.values()) {
      if (user.isTextDone === false) result = false;
    }
    return result;
  }

  setProgress(progress, roomId, user) {
    const users = this.rooms.get(roomId);
    const userToChange = users.get(user);
    userToChange.progress = progress;
    users.set(user, userToChange);
    this.rooms.set(roomId, users);
  }

  setTextDone(roomId, user) {
    const users = this.rooms.get(roomId);
    const userToChange = users.get(user);
    userToChange.isTextDone = true;
    users.set(user, userToChange);
    this.rooms.set(roomId, users);
  }

  getUsersProgress(roomId) {
    const usersArr = this.getRoomUsers(roomId);
    const users = this.rooms.get(roomId);
    const allUsers = [];
    usersArr.forEach(user => {
      let item = [];
      item.push(user);
      item.push(users.get(user).progress);
      allUsers.push(item);
    });
    return allUsers
  }

  getGameResult(roomId) {
    let allUsers = this.getUsersProgress(roomId)
    let sortedUsers = allUsers.sort((a, b) => b[1] - a[1]);
    let usersAll = this.finishedGame[roomId];
    for (let i = this.finishedGame[roomId].length; i < sortedUsers.length; i++)
      usersAll.push(sortedUsers[i][0]);
    return usersAll;
  }

  hasRoom(roomId) {
    return (this.rooms.has(roomId));
  }

  delRoom(roomId) {
    this.rooms.delete(roomId);
  }

}

const db = new Db();

export { db }
