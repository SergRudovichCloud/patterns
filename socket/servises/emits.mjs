import { db } from "./db.mjs";

function finishGame(io, roomId, bot, socket){
    io.in(roomId).emit("BOT", bot.say("GAME_END", roomId));
    db.setAllNotReadyInRoom(roomId);
    const gameResult = db.getGameResult(roomId);
    db.delRoomInGame(roomId);
    io.in(roomId).emit("GAME_END", gameResult);
    socket.broadcast.emit("ROOMS_UPDATE", db.getRooms());
}

export { finishGame }