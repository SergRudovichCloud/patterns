function hasItem(item, storage) {
    for (let i of storage.values()) {
        if (i === item) return true;
    }
    return false;
}

function getRandom(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

export { hasItem, getRandom }